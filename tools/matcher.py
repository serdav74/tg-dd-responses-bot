import logging
from typing import List

from fuzzywuzzy import fuzz

from .voiceline import Voiceline


class MatchResult:
    def __init__(self, line, score):
        self.line = line
        self.score = score

    def __lt__(self, other):
        return self.score < other.score

    def __le__(self, other):
        return self.score <= other.score


class Matcher:
    def __init__(self, lines: List[Voiceline]):
        self.lines = lines
        self.logger = logging.getLogger('app.matcher')
        self.logger.info(f"Created a {self.__class__.__name__} with {len(lines)} line(s).")

    @staticmethod
    def match_score(query, line, weights: List[float] = None):
        if weights is None or len(weights) < 2:
            weights = [0.6, 0.4]

        return weights[0] * fuzz.partial_token_sort_ratio(query, line.text) \
            + weights[1] * fuzz.token_sort_ratio(query, line.text)

    def _find(self, query, min_score):
        self.logger.debug(f"Starting the search for '{query}'...")
        results = []
        for line in self.lines:
            score = self.match_score(query, line)
            if score >= min_score:
                results.append(MatchResult(line, score))
                self.logger.debug(f"[{score:>5.1f}] '{line.text}'")
        results.sort(reverse=True)
        if len(results) == 0:
            self.logger.debug("Nothing found.")
        return results

    def find(self, query: str, min_score: float = 60, max_entries: int = 3):
        self.logger.info(f"Query: '{query}', max_entries: {max_entries}")
        return [r.line for r in self._find(query, min_score)[:max_entries]]

    def find_one(self, query: str, min_score: float = 60, score_diff: float = 10):
        self.logger.info(f"Query: '{query}', one result.")
        results = self._find(query, min_score - score_diff - 1)[:2]
        too_good_threshold = (100.0 + min_score) / 2

        if len(results) == 0:
            return None
        elif len(results) == 1:
            return results[0].line
        elif (results[0].score - results[1].score >= score_diff) or (results[0].score >= too_good_threshold):
            return results[0].line
        else:
            return None
